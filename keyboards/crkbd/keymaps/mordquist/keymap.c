#include QMK_KEYBOARD_H
#include "bootloader.h"
#ifdef PROTOCOL_LUFA
  #include "lufa.h"
  #include "split_util.h"
#endif
#ifdef SSD1306OLED
  #include "ssd1306.h"
#endif

extern keymap_config_t keymap_config;

#ifdef RGBLIGHT_ENABLE
//Following line allows macro to read current RGB settings
extern rgblight_config_t rgblight_config;
#endif

extern uint8_t is_master;

// Each layer gets a name for readability, which is then used in the keymap matrix below.
// The underscores don't mean anything - you can have a layer called STUFF or any other name.
// Layer names don't all need to be of the same length, obviously, and you can also skip them
// entirely and just use numbers.
#define _QWERTY 0
#define _LOWER 1
#define _RAISE 2
#define _ADJUST 3
/*#define _GAMING 5*/
/*#define _GLOWER 6*/
/*#define _GRAISE 7*/
#define _GADJUS 8

enum custom_keycodes {
  QWERTY = SAFE_RANGE,
  LOWER,
  RAISE,
  ADJUST,
  /*GAMING,*/
  /*GLOWER,*/
  /*GRAISE,*/
  /*GADJUS,*/
  BACKLIT,
  RGBRST,
  OS_LNX,
  OS_WIN,
  OS_MAC
};

enum macro_keycodes {
  KC_SAMPLEMACRO,
};

#define KC______ KC_TRNS
#define KC_XXXXX KC_NO
#define KC_LOWER LOWER
#define KC_RAISE RAISE
#define KC_QWERTY QWERTY
/*#define KC_GAMING GAMING*/
/*#define KC_GLOWER GLOWER*/
/*#define KC_GRAISE GRAISE*/
#define KC_RST   RESET
#define KC_LRST  RGBRST
#define KC_LTOG  RGB_TOG
#define KC_LHUI  RGB_HUI
#define KC_LHUD  RGB_HUD
#define KC_LSAI  RGB_SAI
#define KC_LSAD  RGB_SAD
#define KC_LVAI  RGB_VAI
#define KC_LVAD  RGB_VAD
#define KC_LMOD  RGB_MOD
#define KC_NUPI S(KC_NUBS)
#define KC_LNX  OS_LNX
#define KC_WIN  OS_WIN
#define KC_MAC  OS_MAC

#ifdef UNICODE_ENABLE
#undef UNICODE_SELECTED_MODES
#define UNICODE_SELECTED_MODES UC_LNX, UC_WIN
#define KC_ARING UC(0xE5)
#define KC_AUML UC(0xE4)
#define KC_OUML UC(0xF6)
#define KC_CARING UC(0xC5)
#define KC_CAUML UC(0xC4)
#define KC_COUML UC(0xD6)
#endif

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
  [_QWERTY] = LAYOUT_kc( \
  //,-----------------------------------------.                ,-----------------------------------------.
        TAB,     Q,     W,     E,     R,     T,                      Y,     U,     I,     O,     P,  BSPC,\
  //|------+------+------+------+------+------|                |------+------+------+------+------+------|
        ESC,     A,     S,     D,     F,     G,                      H,     J,     K,     L,  SCLN,  QUOT,\
  //|------+------+------+------+------+------|                |------+------+------+------+------+------|
       LSFT,     Z,     X,     C,     V,     B,                      N,     M,  COMM,   DOT,  SLSH,  RSFT,\
  //|------+------+------+------+------+------+------|  |------+------+------+------+------+------+------|
                                  LCTRL, LOWER,   SPC,      ENT, RAISE, LALT  \
                              //`--------------------'  `--------------------'
  ),

  [_RAISE] = LAYOUT_kc( \
  //,-----------------------------------------.                ,-----------------------------------------.
        TAB,     1,     2,     3,     4,     5,                      6,     7,     8,     9,     0,   DEL,\
  //|------+------+------+------+------+------|                |------+------+------+------+------+------|
         F1,    F2,    F3,    F4,    F5,    F6,                   LEFT,  DOWN,    UP, RIGHT, XXXXX, XXXXX,\
  //|------+------+------+------+------+------|                |------+------+------+------+------+------|
         F7,    F8,    F9,   F10,   F11,   F12,                   HOME,  PGDN,  PGUP,   END, XXXXX,  RSFT,\
  //|------+------+------+------+------+------+------|  |------+------+------+------+------+------+------|
                                  LCTRL, LOWER,   SPC,      ENT, RAISE, LALT  \
                              //`--------------------'  `--------------------'
  ),

  [_LOWER] = LAYOUT_kc( \
  //,-----------------------------------------.                ,-----------------------------------------.
        TAB,  EXLM,    AT,  HASH,   DLR,  PERC,                   CIRC,  AMPR,  ASTR,  LPRN,  RPRN,  BSPC,\
  //|------+------+------+------+------+------|                |------+------+------+------+------+------|
        ESC, ARING,  AUML,  OUML, XXXXX,  LCBR,                   RCBR,  MINS,   EQL,  NUBS,  PIPE,   GRV,\
  //|------+------+------+------+------+------|                |------+------+------+------+------+------|
       LSFT,CARING, CAUML, COUML, XXXXX,  LBRC,                   RBRC,  UNDS,  PLUS,  NUPI,  BSLS,  RSFT,\
  //|------+------+------+------+------+------+------|  |------+------+------+------+------+------+------|
                                  LCTL , LOWER,   SPC,      ENT, RAISE, LGUI  \
                              //`--------------------'  `--------------------'
  ),

// ----------------------------- GAMING LAYERS ---------------------------------------

  /*[_GAMING] = LAYOUT_kc( \*/
  /*//,-----------------------------------------.                ,-----------------------------------------.*/
  /*      TAB,     Q,     W,     E,     R,     T,                      Y,     U,     I,     O,     P,  BSPC,\*/
  /*//|------+------+------+------+------+------|                |------+------+------+------+------+------|*/
  /*      ESC,     A,     S,     D,     F,     G,                      H,     J,     K,     L,  SCLN,  QUOT,\*/
  /*//|------+------+------+------+------+------|                |------+------+------+------+------+------|*/
  /*     LSFT,     Z,     X,     C,     V,     B,                      N,     M,  COMM,   DOT,  SLSH,  RSFT,\*/
  /*//|------+------+------+------+------+------+------|  |------+------+------+------+------+------+------|*/
  /*                                LCTRL,GLOWER,   SPC,      ENT,GRAISE, LALT  \*/
  /*                            //`--------------------'  `--------------------'*/
  /*),*/

  /*[_GRAISE] = LAYOUT_kc( \*/
  /*//,-----------------------------------------.                ,-----------------------------------------.*/
  /*      TAB,     1,     2,     3,     4,     5,                      6,     7,     8,     9,     0,   DEL,\*/
  /*//|------+------+------+------+------+------|                |------+------+------+------+------+------|*/
  /*       F1,    F2,    F3,    F4,    F5,    F6,                   LEFT,  DOWN,    UP, RIGHT, XXXXX, XXXXX,\*/
  /*//|------+------+------+------+------+------|                |------+------+------+------+------+------|*/
  /*       F7,    F8,    F9,   F10,   F11,   F12,                   HOME,  PGDN,  PGUP,   END, XXXXX,  RSFT,\*/
  /*//|------+------+------+------+------+------+------|  |------+------+------+------+------+------+------|*/
  /*                                LCTRL,GLOWER,   SPC,      ENT,GRAISE, LALT  \*/
  /*                            //`--------------------'  `--------------------'*/
  /*),*/

  /*[_GLOWER] = LAYOUT_kc( \*/
  /*//,-----------------------------------------.                ,-----------------------------------------.*/
  /*      TAB,     1,     2,     3,     4,     5,                      6,     7,     8,     9,     0,   DEL,\*/
  /*//|------+------+------+------+------+------|                |------+------+------+------+------+------|*/
  /*      ESC,     6,     7,     8,     9,     0,                   RCBR,  MINS,   EQL,  NUBS,  PIPE,   GRV,\*/
  /*//|------+------+------+------+------+------|                |------+------+------+------+------+------|*/
  /*     LSFT, XXXXX, XXXXX, XXXXX, XXXXX, XXXXX,                   RBRC,  UNDS,  PLUS,  NUPI,  BSLS,  RSFT,\*/
  /*//|------+------+------+------+------+------+------|  |------+------+------+------+------+------+------|*/
  /*                                LCTL ,GLOWER,   SPC,      ENT,GRAISE, LGUI  \*/
  /*                            //`--------------------'  `--------------------'*/
  /*),*/

  /*[_GADJUS] = LAYOUT_kc( \*/
  /*//,-----------------------------------------.                ,-----------------------------------------.*/
  /*      RST,  LRST, XXXXX, XXXXX, XXXXX, XXXXX,                  XXXXX,   LNX,   WIN,QWERTY,GAMING, XXXXX,\*/
  /*//|------+------+------+------+------+------|                |------+------+------+------+------+------|*/
  /*     LTOG,  LHUI,  LSAI,  LVAI, XXXXX, XXXXX,                  XXXXX, XXXXX, XXXXX, XXXXX, XXXXX, XXXXX,\*/
  /*//|------+------+------+------+------+------|                |------+------+------+------+------+------|*/
  /*     LMOD,  LHUD,  LSAD,  LVAD, XXXXX, XXXXX,                  XXXXX, XXXXX, XXXXX, XXXXX, XXXXX, XXXXX,\*/
  /*//|------+------+------+------+------+------+------|  |------+------+------+------+------+------+------|*/
  /*                                LGUI , LOWER,   SPC,      ENT, RAISE, LALT  \*/
  /*                            //`--------------------'  `--------------------'*/
  /*),*/


// -----------------------------------------------------------------------------------


  [_ADJUST] = LAYOUT_kc( \
  //,-----------------------------------------.                ,-----------------------------------------.
        RST,  LRST, XXXXX, XXXXX, XXXXX, XXXXX,                  XXXXX,   LNX,   WIN,QWERTY, XXXXX, XXXXX,\
  //|------+------+------+------+------+------|                |------+------+------+------+------+------|
       LTOG,  LHUI,  LSAI,  LVAI, XXXXX, XXXXX,                  XXXXX, XXXXX, XXXXX, XXXXX, XXXXX, XXXXX,\
  //|------+------+------+------+------+------|                |------+------+------+------+------+------|
       LMOD,  LHUD,  LSAD,  LVAD, XXXXX, XXXXX,                  XXXXX, XXXXX, XXXXX, XXXXX, XXXXX, XXXXX,\
  //|------+------+------+------+------+------+------|  |------+------+------+------+------+------+------|
                                  LGUI , LOWER,   SPC,      ENT, RAISE, LALT  \
                              //`--------------------'  `--------------------'
  )
};

int RGB_current_mode;

void persistent_default_layer_set(uint16_t default_layer) {
  eeconfig_update_default_layer(default_layer);
  default_layer_set(default_layer);
}

// Setting ADJUST layer RGB back to default
void update_tri_layer_RGB(uint8_t layer1, uint8_t layer2, uint8_t layer3) {
  if (IS_LAYER_ON(layer1) && IS_LAYER_ON(layer2)) {
    layer_on(layer3);
  } else {
    layer_off(layer3);
  }
}

void eeconfig_init_user(void) {
}

void matrix_init_user(void) {
    persistent_default_layer_set(0); // safety to always reset default layer to qwerty
    #ifdef UNICODE_ENABLE
      set_unicode_input_mode(UC_LNX);
    #endif
    #ifdef RGBLIGHT_ENABLE
      RGB_current_mode = rgblight_config.mode;
    #endif
    //SSD1306 OLED init, make sure to add #define SSD1306OLED in config.h
    #ifdef SSD1306OLED
        iota_gfx_init(!has_usb());   // turns on the display
    #endif
}

//SSD1306 OLED update loop, make sure to add #define SSD1306OLED in config.h
#ifdef SSD1306OLED

// When add source files to SRC in rules.mk, you can use functions.
const char *read_layer_state(void);
const char *read_logo(void);
//void set_keylog(uint16_t keycode, keyrecord_t *record);
//const char *read_keylog(void);
//const char *read_keylogs(void);

const char *read_mode_icon(bool swap);
const char *read_host_led_state(void);
const char *read_rgb_info(void);
// void set_timelog(void);
// const char *read_timelog(void);
#ifdef UNICODE_ENABLE
  uint8_t get_unicode_input_mode(void);
#endif

void matrix_scan_user(void) {
   iota_gfx_task();
}

void matrix_render_user(struct CharacterMatrix *matrix) {
  if (is_master) {
    // If you want to change the display of OLED, you need to change here
    matrix_write_ln(matrix, read_layer_state());
    //matrix_write_ln(matrix, read_keylog());
    //matrix_write_ln(matrix, read_keylogs());
    //matrix_write_ln(matrix, (get_unicode_input_mode());
    matrix_write_ln(matrix, read_host_led_state());
    matrix_write_ln(matrix, read_rgb_info());
    //matrix_write_ln(matrix, read_timelog());
  } else {
    if (IS_LAYER_ON(_ADJUST)) {
      matrix_write_ln(matrix, read_mode_icon(get_unicode_input_mode() != 1));
    } else {
      matrix_write(matrix, read_logo());
    }
  }
}

void matrix_update(struct CharacterMatrix *dest, const struct CharacterMatrix *source) {
  if (memcmp(dest->display, source->display, sizeof(dest->display))) {
    memcpy(dest->display, source->display, sizeof(dest->display));
    dest->dirty = true;
  }
}

void iota_gfx_task_user(void) {
  struct CharacterMatrix matrix;
  matrix_clear(&matrix);
  matrix_render_user(&matrix);
  matrix_update(&display, &matrix);
}
#endif//SSD1306OLED

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  if (record->event.pressed) {
#ifdef SSD1306OLED
    //set_keylog(keycode, record);
#endif
    // set_timelog();
  }

  switch (keycode) {
    case QWERTY:
      if (record->event.pressed) {
        persistent_default_layer_set(1UL<<_QWERTY);
      }
      return false;
      break;
    /*case GAMING:*/
    /*  if (record->event.pressed) {*/
    /*    persistent_default_layer_set(1UL<<_GAMING);*/
    /*  }*/
    /*  return false;*/
    /*  break;*/
    case LOWER:
      if (record->event.pressed) {
        layer_on(_LOWER);
        update_tri_layer_RGB(_LOWER, _RAISE, _ADJUST);
      } else {
        layer_off(_LOWER);
        update_tri_layer_RGB(_LOWER, _RAISE, _ADJUST);
      }
      return false;
      break;
    case RAISE:
      if (record->event.pressed) {
        layer_on(_RAISE);
        update_tri_layer_RGB(_LOWER, _RAISE, _ADJUST);
      } else {
        layer_off(_RAISE);
        update_tri_layer_RGB(_LOWER, _RAISE, _ADJUST);
      }
      return false;
      break;
    /*case GLOWER:*/
    /*  if (record->event.pressed) {*/
    /*    layer_on(_GLOWER);*/
    /*    update_tri_layer_RGB(_GLOWER, _GRAISE, _GADJUS);*/
    /*  } else {*/
    /*    layer_off(_GLOWER);*/
    /*    update_tri_layer_RGB(_GLOWER, _GRAISE, _GADJUS);*/
    /*  }*/
    /*  return false;*/
    /*  break;*/
    /*case GRAISE:*/
    /*  if (record->event.pressed) {*/
    /*    layer_on(_GRAISE);*/
    /*    update_tri_layer_RGB(_GLOWER, _GRAISE, _GADJUS);*/
    /*  } else {*/
    /*    layer_off(_GRAISE);*/
    /*    update_tri_layer_RGB(_GLOWER, _GRAISE, _GADJUS);*/
    /*  }*/
    /*  return false;*/
    /*  break;*/
    case ADJUST:
        if (record->event.pressed) {
          layer_on(_ADJUST);
        } else {
          layer_off(_ADJUST);
        }
        return false;
        break;
    case RGB_MOD:
      #ifdef RGBLIGHT_ENABLE
        if (record->event.pressed) {
          rgblight_mode(RGB_current_mode);
          rgblight_step();
          RGB_current_mode = rgblight_config.mode;
        }
      #endif
      return false;
      break;
    case RGBRST:
      #ifdef RGBLIGHT_ENABLE
        if (record->event.pressed) {
          eeconfig_update_rgblight_default();
          rgblight_enable();
          RGB_current_mode = rgblight_config.mode;
        }
      #endif
      return false;
      break;

#ifdef UNICODE_ENABLE
    case OS_LNX:
      set_unicode_input_mode(UC_LNX);
      return false;
      break;
    case OS_WIN:
      set_unicode_input_mode(UC_WINC);
      return false;
      break;
    case OS_MAC:
      set_unicode_input_mode(UC_OSX);
      return false;
      break;
#endif

  }
  return true;
}

