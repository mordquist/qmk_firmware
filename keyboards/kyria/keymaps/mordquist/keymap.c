/* Copyright 2019 Thomas Baart <thomas@splitkb.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include QMK_KEYBOARD_H

enum layers {
    _QWERTY = 0,
    _LOWER,
    _RAISE,
    _ADJUST
};

#define KC_XXXX KC_NO
#define KC_____ KC_TRNS
#define KC_NUPI S(KC_NUBS)
#define KC_RAISE MO(_RAISE)
#define KC_LOWER MO(_LOWER)
#define KC_CTLESC MT(MOD_LCTL, KC_ESC)

#ifdef UNICODE_ENABLE
#undef UNICODE_SELECTED_MODES
#define UNICODE_SELECTED_MODES UC_LNX, UC_WINC, UC_MAC
#define KC_LNX UNICODE_MODE_LNX
#define KC_WIN UNICODE_MODE_WINC
#define KC_MAC UNICODE_MODE_MAC
#define KC_ARING UC(0xE5)
#define KC_AUML UC(0xE4)
#define KC_OUML UC(0xF6)
#define KC_EACT UC(0xE9)
#define KC_CARING UC(0xC5)
#define KC_CAUML UC(0xC4)
#define KC_COUML UC(0xD6)
#define KC_CEACT UC(0xC9)
#endif

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
 //Base Layer: QWERTY
    [_QWERTY] = LAYOUT_kc(
 //,-------------------------------------------.                              ,-------------------------------------------.
      TAB   ,  Q   ,   W  ,   E  ,   R  ,   T  ,                                  Y  ,   U  ,   I  ,  O   ,  P   ,  BSPC  ,
 //|--------+------+------+------+------+------|                              |------+------+------+------+------+--------|
     CTLESC ,  A   ,   S  ,   D  ,   F ,    G  ,                                  H  ,   J  ,   K  ,  L   , SCLN ,  QUOT  ,
 //|--------+------+------+------+------+------+-------------.  ,-------------+------+------+------+------+------+--------|
      LSFT,    Z   ,   X  ,   C  ,   V  ,   B  , LSFT , LSFT ,    LSFT , LSFT ,   N  ,  M   , COMM ,  DOT , SLSH ,  RSFT  ,
 //`----------------------+------+------+------+------+------|  |------+------+------+------+------+----------------------'
                            LGUI , LCTL , LOWER,  SPC , SPC  ,    ENT  , ENT  , RAISE, BSPC , RALT
 //                       `----------------------------------'  `----------------------------------'
    ),

 // Lower Layer: Symbols
    [_LOWER] = LAYOUT_kc(
 // ,-------------------------------------------.                              ,-------------------------------------------.
       TILD  , EXLM ,  AT  , HASH ,  DLR , PERC ,                                CIRC , AMPR , ASTR , LPRN , RPRN , DEL    ,
 // |--------+------+------+------+------+------|                              |------+------+------+------+------+--------|
       ____  , ARING, AUML , OUML , EACT , LCBR ,                                RCBR , MINS , EQL  , NUBS , PIPE , TILD   ,
 // `----------------------+------+------+------+------+------|  |------+------+------+------+------+----------------------'
       ____  ,CARING, CAUML, COUML, CEACT, LBRC , ____ , ____ ,    ____ , ____ , RBRC , UNDS , PLUS , NUPI , BSLS ,  ____  ,
 // |--------+------+------+------+------+------+-------------.  ,-------------+------+------+------+------+------+--------|
                             ____ , ____ , ____ , SCLN ,  EQL ,    EQL  , SCLN , ____ , ____ , ____
 //                        `----------------------------------'  `----------------------------------'
    ),

 // Raise Layer: Number keys, navigation
    [_RAISE] = LAYOUT_kc(
 // ,-------------------------------------------.                              ,-------------------------------------------.
        GRV  ,  1   ,   2  ,  3   ,  4   ,  5   ,                                  6  ,  7   ,  8   ,  9   ,  0   ,  DEL   ,
 // |--------+------+------+------+------+------|                              |------+------+------+------+------+--------|
        F1   ,  F2  ,  F3  ,  F4  ,  F5  ,  F6  ,                                LEFT , DOWN ,  UP  , RGHT , ____ ,  ____  ,
 // |--------+------+------+------+------+------+-------------.  ,-------------+------+------+------+------+------+--------|
        F7   ,  F8  ,  F9  ,  F10 ,  F11 ,  F12 , ____ , ____ ,    ____ , ____ , HOME , PGDN , PGUP ,  END , ____ ,  ____  ,
 // `----------------------+------+------+------+------+------|  |------+------+------+------+------+----------------------'
                             ____ , ____ , ____ , ____ , ____ ,    ____ , ____ , ____ , ____ , ____
 //                        `----------------------------------'  `----------------------------------'
    ),
 // Adjust Layer: Function keys, RGB
    [_ADJUST] = LAYOUT_kc(
 // ,-------------------------------------------.                              ,-------------------------------------------.
       XXXX  , XXXX , XXXX , XXXX , XXXX , XXXX ,                                XXXX ,  LNX ,  WIN ,  MAC , XXXX ,  XXXX  ,
 // |--------+------+------+------+------+------|                              |------+------+------+------+------+--------|
       XXXX  , XXXX , XXXX , XXXX , XXXX , XXXX ,                                XXXX , XXXX , XXXX , XXXX , XXXX ,  XXXX  ,
 // |--------+------+------+------+------+------+-------------.  ,-------------+------+------+------+------+------+--------|
       XXXX  , XXXX , XXXX , XXXX , XXXX , XXXX , XXXX , XXXX ,    XXXX , XXXX , XXXX , XXXX , XXXX , XXXX , XXXX ,  XXXX  ,
 // `----------------------+------+------+------+------+------|  |------+------+------+------+------+----------------------'
                             ____ , ____ , ____ , ____ , ____ ,    ____ , ____ , ____ , ____ , ____
 //                        `----------------------------------'  `----------------------------------'
    ),
};

layer_state_t layer_state_set_user(layer_state_t state) {
    return update_tri_layer_state(state, _LOWER, _RAISE, _ADJUST);
}
