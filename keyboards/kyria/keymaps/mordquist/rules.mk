OLED_DRIVER_ENABLE = no   # Enables the use of OLED displays
ENCODER_ENABLE = no       # Enables the use of one or more encoders
RGBLIGHT_ENABLE = no      # Enable keyboard RGB underglow
BACKLIGHT_ENABLE = no      # Enable keyboard backlight functionality on B7 by default
UNICODE_ENABLE = yes      # Unicode
MOUSEKEY_ENABLE = no      # Mouse keys
# if this doesn't work, see here: https://github.com/tmk/tmk_keyboard/wiki/FAQ#nkro-doesnt-work
NKRO_ENABLE = yes         # USB Nkey Rollover
