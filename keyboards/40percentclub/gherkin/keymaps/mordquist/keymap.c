#include QMK_KEYBOARD_H

enum keyboard_layers {
  _QWERTY,
  _RAISE,
  _LOWER,
  _ADJUST
};

#define ADJ_Z   LT(_ADJUST, KC_Z)
#define RS_BSPC LT(_RAISE, KC_BSPC)
#define LW_SPC  LT(_LOWER, KC_SPC)
#define MO_ADJ  MO(_ADJUST)
#define OS_LCTL OSM(MOD_LCTL)
#define OS_LALT OSM(MOD_LALT)
#define OS_LGUI OSM(MOD_LGUI)
#define OS_RCTL OSM(MOD_RCTL)
#define OS_RALT OSM(MOD_RALT)
#define OS_RGUI OSM(MOD_RGUI)

#ifdef UNICODE_ENABLE
#undef UNICODE_SELECTED_MODES
#define UNICODE_SELECTED_MODES UC_LINX, UC_WINC, UC_MAC
#define ARING UC(0xE5)
#define AUML UC(0xE4)
#define OUML UC(0xF6)
#endif

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

  [_QWERTY] = LAYOUT_ortho_3x10(
    KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,    KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,
    KC_A,    KC_S,    KC_D,    KC_F,    KC_G,    KC_H,    KC_J,    KC_K,    KC_L,    MT(MOD_RSFT, KC_ESC),
    ADJ_Z,   KC_X,    KC_C,    KC_V,    LW_SPC,  RS_BSPC, KC_B,    KC_N,    KC_M,    MT(MOD_RCTL, KC_ENT)
  ),

  [_RAISE] = LAYOUT_ortho_3x10(
    KC_1,    KC_2,    KC_3,    KC_4,    KC_5,    KC_6,    KC_7,    KC_8,    KC_9,    KC_0,
    KC_ESC,  KC_MINS, KC_EQL,  KC_LBRC, KC_RBRC, KC_DQUO, KC_QUOT, KC_COLN, KC_SCLN, _______,
    OS_LCTL, OS_LGUI, OS_LALT, KC_GRV,  KC_TAB,  _______, KC_BSLS, KC_LT, KC_GT,   KC_QUES
  ),

  [_LOWER] = LAYOUT_ortho_3x10(
    KC_EXLM, KC_AT,   KC_HASH, KC_DLR,  KC_PERC, KC_CIRC, KC_AMPR, KC_ASTR, KC_LPRN, KC_RPRN,
    KC_ESC,  KC_UNDS, KC_PLUS, KC_LCBR, KC_RCBR, KC_LEFT, KC_DOWN, KC_UP,   KC_RGHT, _______,
    KC_NUBS,S(KC_NUBS),_______,KC_TILD, _______, KC_DEL,  KC_PIPE, KC_COMM, KC_DOT,  KC_SLSH 
  ),

//  [_PLOVER] = LAYOUT_ortho_3x10(
//    STN_S1,  STN_TL,  STN_PL,  STN_HL,  STN_ST1, STN_FR,  STN_PR,  STN_LR,  STN_TR,  STN_DR,
//    STN_S2,  STN_KL,  STN_WL,  STN_RL,  STN_ST2, STN_RR,  STN_BR,  STN_GR,  STN_SR,  STN_ZR,
//    MO_ADJ,  STN_NUM, STN_A,   STN_O,   STN_NUM, STN_E,   STN_U,   STN_NUM, STN_NUM, XXXXXXX
//  ),

  [_ADJUST] = LAYOUT_ortho_3x10(
    KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_F6,   KC_F7,   KC_F8,   KC_F9,   KC_F10,
    ARING,   AUML,    OUML,    XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, KC_F11,  KC_F12,
    XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, QK_REBOOT
  ),

};

void matrix_init_user(void) {
}
