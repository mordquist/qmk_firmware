#include QMK_KEYBOARD_H
#include "keymap_swedish_pro_mac_iso.h"

#ifdef PROTOCOL_LUFA
  #include "lufa.h"
  #include "split_util.h"
#endif
#ifdef SSD1306OLED
  #include "ssd1306.h"
#endif

extern uint8_t is_master;

extern keymap_config_t keymap_config;

#ifdef RGBLIGHT_ENABLE
//Following line allows macro to read current RGB settings
extern rgblight_config_t rgblight_config;
#endif

#ifdef WPM_ENABLE
char wpm_str[10];
#endif


#define _QWERTY 0
#define _LOWER 1
#define _RAISE 2
#define _ADJUST 3

#define KC_RAISE RAISE
#define KC_LOWER LOWER
#define KC_RESET RESET
#define KC_XXXX KC_NO
#define KC_____ KC_TRNS
#define KC_NUPI S(KC_NUBS)

#define KC_RGB_TOG RGB_TOG
#define KC_RGB_HUI RGB_HUI
#define KC_RGB_SAI RGB_SAI
#define KC_RGB_VAI RGB_VAI
#define KC_RGB_MOD RGB_MOD
#define KC_RGB_HUD RGB_HUD
#define KC_RGB_SAD RGB_SAD
#define KC_RGB_VAD RGB_VAD

#ifdef UNICODE_ENABLE
#define KC_LINX UC_LINX
#define KC_WINC UC_WINC
#define KC_MAC  UC_MAC
#undef UNICODE_SELECTED_MODES
#define UNICODE_SELECTED_MODES UC_MAC, UC_LINX, UC_WINC
#define KC_ARING UC(0xE5)
#define KC_AUML UC(0xE4)
#define KC_OUML UC(0xF6)
#define KC_EACT UC(0xE9)
#define KC_CARING UC(0xC5)
#define KC_CAUML UC(0xC4)
#define KC_COUML UC(0xD6)
#define KC_CEACT UC(0xC9)
#endif


enum custom_keycodes {
  QWERTY = SAFE_RANGE,
  LOWER,
  RAISE,
  ADJUST,
  OS_LINX,
  OS_WINC,
  OS_MAC,
  OS_EMAC,
};

// Remove need for KC_ prefix in keymaps
#define LAYOUT_kc( \
	L00, L01, L02, L03, L04, L05,           R00, R01, R02, R03, R04, R05,  \
	L10, L11, L12, L13, L14, L15,           R10, R11, R12, R13, R14, R15,  \
	L20, L21, L22, L23, L24, L25,           R20, R21, R22, R23, R24, R25,  \
	L30, L31, L32, L33, L34, L35, L45, R40, R30, R31, R32, R33, R34, R35, \
	               L41, L42, L43, L44, R41, R42, R43, R44  \
  ) \
  LAYOUT( \
	KC_##L00, KC_##L01, KC_##L02, KC_##L03, KC_##L04, KC_##L05,           KC_##R00, KC_##R01, KC_##R02, KC_##R03, KC_##R04, KC_##R05,  \
	KC_##L10, KC_##L11, KC_##L12, KC_##L13, KC_##L14, KC_##L15,           KC_##R10, KC_##R11, KC_##R12, KC_##R13, KC_##R14, KC_##R15,  \
	KC_##L20, KC_##L21, KC_##L22, KC_##L23, KC_##L24, KC_##L25,           KC_##R20, KC_##R21, KC_##R22, KC_##R23, KC_##R24, KC_##R25,  \
	KC_##L30, KC_##L31, KC_##L32, KC_##L33, KC_##L34, KC_##L35, KC_##L45, KC_##R40, KC_##R30, KC_##R31, KC_##R32, KC_##R33, KC_##R34, KC_##R35, \
	               KC_##L41, KC_##L42, KC_##L43, KC_##L44, KC_##R41, KC_##R42, KC_##R43, KC_##R44  \
  )

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

 [_QWERTY] = LAYOUT_kc( \
// ,-----------------------------------------.                    ,-----------------------------------------.
     MINS ,   1  ,   2  ,   3  ,   4  ,   5  ,                        6  ,   7  ,   8  ,   9  ,   0  , BSPC , \
// |------+------+------+------+------+------|                    |------+------+------+------+------+------|
     ESC  ,   Q  ,   W  ,   E  ,   R  ,   T  ,                        Y  ,   U  ,   I  ,   O  ,   P  , LBRC , \
// |------+------+------+------+------+------|                    |------+------+------+------+------+------|
     TAB  ,   A  ,   S  ,   D  ,   F  ,   G  ,                        H  ,   J  ,   K  ,   L  , SCLN , QUOT , \
// |------+------+------+------+------+------+-------.    ,-------|------+------+------+------+------+------|
     LSFT ,   Z  ,   X  ,   C  ,   V  ,   B  , LOWER ,       RBRC ,   N  ,   M  , COMM , DOT  , SLSH , RSFT , \
// `-----------------------------------------/-------/     \------\-----------------------------------------'
                       LCTL , LOPT , LCMD ,  SPC    ,         ENT  ,   RAISE , RCMD , ROPT \
//                   `------'------'------'-------'           '------''------'------'------'

),
[_LOWER] = LAYOUT_kc( \
// ,-----------------------------------------.                    ,-----------------------------------------.
     ____ , ____ , ____ , ____ , ____ , ____ ,                      ____ , ____ , ____ , ____ , ____ , DEL  , \
// |------+------+------+------+------+------|                    |------+------+------+------+------+------|
     XXXX , XXXX , XXXX ,  UP  , XXXX , XXXX ,                      CIRC , XXXX ,  UP  , XXXX , HOME , PGUP , \
// |------+------+------+------+------+------|                    |------+------+------+------+------+------|
     XXXX , XXXX , LEFT , DOWN , RGHT , XXXX ,                       INS , LEFT , DOWN , RGHT , END  , PGDN , \
// |------+------+------+------+------+------+-------.    ,-------|------+------+------+------+------+------|
     XXXX , XXXX , XXXX , XXXX , XXXX , XXXX ,  ____ ,       ____ , PSCR , XXXX , XXXX , XXXX , XXXX , RSFT , \
// `-----------------------------------------/-------/     \------\-----------------------------------------'
                       ____ , ____ , ____ ,  ____   ,         ____ ,   ____  , RCTL , ____ \
//                   `------'------'------'-------'           '------''------'------'------'
),
[_RAISE] = LAYOUT_kc( \
// ,-----------------------------------------.                    ,-----------------------------------------.
      F11 ,  F1  ,  F2  ,  F3  ,  F4  ,  F5  ,                       F6  ,  F7  ,  F8  ,  F9  ,  F10 ,  F12 , \
// |------+------+------+------+------+------|                    |------+------+------+------+------+------|
     XXXX , XXXX , XXXX ,  UP  , XXXX , XXXX ,                      XXXX , XXXX ,  UP  , XXXX , HOME , PGUP , \
// |------+------+------+------+------+------|                    |------+------+------+------+------+------|
     XXXX , XXXX , LEFT , DOWN , RGHT , XXXX ,                      XXXX , LEFT , DOWN , RGHT , END  , PGDN , \
// |------+------+------+------+------+------+-------.    ,-------|------+------+------+------+------+------|
     XXXX , XXXX , XXXX , XXXX , XXXX , XXXX ,  ____ ,       ____ , XXXX , XXXX , XXXX , XXXX , XXXX , RSFT , \
// `-----------------------------------------/-------/     \------\-----------------------------------------'
                       ____ , ____ , ____ ,  ____   ,         ____ ,    ____ , ____ , ____ \
//                   `------'------'------'-------'           '------''------'------'------'
),
[_ADJUST] = LAYOUT_kc( \
// ,-----------------------------------------.                    ,-----------------------------------------.
     XXXX , XXXX , XXXX , XXXX , XXXX , XXXX ,                      XXXX , XXXX , XXXX , XXXX , XXXX , XXXX , \
// |------+------+------+------+------+------|                    |------+------+------+------+------+------|
     XXXX , XXXX , XXXX , XXXX , XXXX , XXXX ,                      XXXX , XXXX , XXXX , XXXX , XXXX , XXXX , \
// |------+------+------+------+------+------|                    |------+------+------+------+------+------|
     XXXX , XXXX , XXXX , XXXX , XXXX , XXXX ,                      XXXX , XXXX , XXXX , XXXX , XXXX , XXXX , \
// |------+------+------+------+------+------+-------.    ,-------|------+------+------+------+------+------|
     XXXX , XXXX , XXXX , XXXX , XXXX , XXXX ,  ____ ,       ____ , XXXX , XXXX , XXXX , XXXX , XXXX , XXXX , \
// `-----------------------------------------/-------/     \------\-----------------------------------------'
                       ____ , ____ , ____ ,  ____  ,         ____  ,    ____ , ____ , ____ \
//                   `----------------------------'           '------''--------------------'
  )
};

void matrix_init_user(void) {
    #ifdef RGBLIGHT_ENABLE
      RGB_current_mode = rgblight_config.mode;
    #endif
}

//SSD1306 OLED update loop, make sure to add #define SSD1306OLED in config.h
#ifdef OLED_ENABLE

oled_rotation_t oled_init_user(oled_rotation_t rotation) {
  if (!is_keyboard_master())
    return OLED_ROTATION_180;  // flips the display 180 degrees if offhand
  return rotation;
}

// When add source files to SRC in rules.mk, you can use functions.
const char *read_layer_state(void);
const char *read_logo(void);
//void set_keylog(uint16_t keycode, keyrecord_t *record);
//const char *read_keylog(void);
//const char *read_keylogs(void);

const char *read_mode_icon(int mode);
const char *read_host_led_state(void);
void render_anim(void);
// void set_timelog(void);
// const char *read_timelog(void);

bool oled_task_user(void) {
  if (is_keyboard_master()) {
    // If you want to change the display of OLED, you need to change here
    oled_write_ln(read_layer_state(), false);
    oled_write_ln(read_host_led_state(), false);
    oled_write_ln("", false);
    return false;
  } else {
    //oled_write(read_logo(), false);

    render_anim();  // renders pixelart

    oled_set_cursor(0, 0);                            // sets cursor to (row, column) using charactar spacing (5 rows on 128x32 screen, anything more will overflow back to the top)
    sprintf(wpm_str, "WPM:%03d", get_current_wpm());  // edit the string to change wwhat shows up, edit %03d to change how many digits show up
    oled_write(wpm_str, false);                       // writes wpm on top left corner of string
    return false;
  }
}
#endif // OLED_ENABLE

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
#ifdef OLED_ENABLE
  if (record->event.pressed) {
    // set_keylog(keycode, record);
    // set_timelog();
  }
#endif

  switch (keycode) {
    case QWERTY:
      if (record->event.pressed) {
        set_single_persistent_default_layer(_QWERTY);
      }
      return false;
      break;
    case LOWER:
      if (record->event.pressed) {
        layer_on(_LOWER);
        update_tri_layer(_LOWER, _RAISE, _ADJUST);
      } else {
        layer_off(_LOWER);
        update_tri_layer(_LOWER, _RAISE, _ADJUST);
      }
      return false;
      break;
    case RAISE:
      if (record->event.pressed) {
        layer_on(_RAISE);
        update_tri_layer(_LOWER, _RAISE, _ADJUST);
      } else {
        layer_off(_RAISE);
        update_tri_layer(_LOWER, _RAISE, _ADJUST);
      }
      return false;
      break;
    case ADJUST:
        if (record->event.pressed) {
          layer_on(_ADJUST);
        } else {
          layer_off(_ADJUST);
        }
        return false;
        break;
  }
  return true;
}
