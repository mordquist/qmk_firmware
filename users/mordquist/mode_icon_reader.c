#include <stdio.h>
#include "mode_icon_reader.h"

char mode_icon[24];

const char *read_mode_icon(int mode) {
  static char logo[][2][3] = {{{0x99, 0x9a, 0}, {0xb9, 0xba, 0}},  // Linux Logo
                              {{0x97, 0x98, 0}, {0xb7, 0xb8, 0}},  // Windows Logo
                              {{0x95, 0x96, 0}, {0xb5, 0xb6, 0}}}; // macOS Logo
  if (mode == 1) {
    snprintf(mode_icon, sizeof(mode_icon), "%s\n%s", logo[0][0], logo[0][1]);
  } else if (mode == 4) {
    snprintf(mode_icon, sizeof(mode_icon), "%s\n%s", logo[1][0], logo[1][1]);
  } else if (mode == 0) {
    snprintf(mode_icon, sizeof(mode_icon), "%s\n%s", logo[2][0], logo[2][1]);
  } else if (mode == 5) {
    snprintf(mode_icon, sizeof(mode_icon), "Emacs\n"); // no logo for emacs currently :(
  } else {
    snprintf(mode_icon, sizeof(mode_icon), "unknown: %i\n", mode); //print out mode number
  }

  return mode_icon;
}
