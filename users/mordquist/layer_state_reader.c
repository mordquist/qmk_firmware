#include QMK_KEYBOARD_H
#include <stdio.h>
#include "layer_state_reader.h"

#define L_BASE 0
#define L_LOWER (1 << 2)
#define L_RAISE (1 << 3)
#define L_ADJUST (1 << 4)
#define L_ADJUST_TRI (L_ADJUST | L_RAISE | L_LOWER)

char layer_state_str[24];

const char *read_layer_state(void) {
  switch (layer_state)
  {
  case L_BASE:
    if(default_layer_state == 1)
      snprintf(layer_state_str, sizeof(layer_state_str), "Layer: Qwerty");
    else
      snprintf(layer_state_str, sizeof(layer_state_str), "Layer: Workman");
    break;
  case L_RAISE:
    snprintf(layer_state_str, sizeof(layer_state_str), "Layer: Raise");
    break;
  case L_LOWER:
    snprintf(layer_state_str, sizeof(layer_state_str), "Layer: Lower");
    break;
  case L_ADJUST:
  case L_ADJUST_TRI:
    snprintf(layer_state_str, sizeof(layer_state_str), "Layer: Adjust");
    break;
  default:
    snprintf(layer_state_str, sizeof(layer_state_str), "Layer: Undef-%d", layer_state);
  }

  return layer_state_str;
}
